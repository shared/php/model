<?php

declare(strict_types=1);

namespace Naker\Model;

use DateTimeInterface;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
trait FormatDateModel
{
    /**
     * @param DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }
}
